import React, {Component} from 'react'

export default class Results extends Component {
  render () {
    let error = true
    let errorHTML = undefined
    let results = ['search example', 'ex 2', 'ex 3']

    if (error) {
      errorHTML = (
        <div className="col-sm-4 col-md-offset-1">
          <div className="alert alert-warning" role="alert">No results found</div>
        </div>
      )
    }

    return (
      <div>
        <div id="search-results" className="col-md-10 col-md-offset-1">
          {results.forEach(item => {
            console.log(item)
            return (<div>{item}</div>)
          })}
        </div>

        {errorHTML}
      </div>
    )
  }
}
