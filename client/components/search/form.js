import React, {Component} from 'react'

export default class SearchForm extends Component {
  render () {

    let searchDisabled = false 
    let searchDisabledHtml = undefined 
    let searchD = undefined 


        // <br />
        // <br />
    if (searchDisabled) {
      searchDisabledHtml = (
        <div className="alert alert-danger" role="alert">
          Searching has been disabled.
        </div>
      )

      searchD = 'disabled=\"\"'
    }

    let searching = undefined 

    if (searching) {
      searching = (<i className="fa fa-spinner fa-pulse"></i>)
    } else {
      searching = (<i className="fa fa-search"></i>)
    }

    return (
      <form className="form-inline" id="search-form">
        <div className="form-group">
          <div className="input-group">
            <input type="text" className="form-control" id="search-input" size="35" placeholder="Search" searchD autofocus="" />
            <div className="input-group-addon">
              {searching}
            </div>
          </div>
        </div>
          {searchDisabledHtml}
      </form>
    )
  }
}