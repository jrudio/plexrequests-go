import React, {Component} from 'react'
import {Link} from 'react-router'

import SearchForm from './form'
import Results from './results'

export default class Search extends Component {
  render () {
    let error = true



    return (
      <div className="search">
          <div className="row">
            <div className="col-md-10 col-md-offset-1">
              <h1>Search</h1>
              <p>Want to watch something but it's not currently on Plex? Select an option below and do a quick search!</p>

              {<SearchForm />}

              {/* Search selector */}
              {/*<ul className="search-selector">
                              {{#each searchOptions}}
                              <li><a href="#" className="{{#if activeSearch}}active-search{{/if}}">{{this}}</a></li>
                              {{/each}}
                            </ul>*/}

            </div>
          </div>
          <div className="row">
            {<Results />}
          </div>

          {/* Go to top button */}
          <div className="go-to-top">
            <i className="fa fa-arrow-up"></i>
          </div>

        </div>
    )
  }
}