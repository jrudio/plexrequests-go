import React, {Component} from 'react'
import {Link} from 'react-router'

import Navigation from '../navigation'

export default class Layout extends Component {
  render () {
    return (
      <div>
        {<Navigation />}


        <div className="container">
          {this.props.children}
        </div>

        <div className="modal fade" id="first-run-modal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  <span className="sr-only">Close</span>
                </button>
                <h4 className="modal-title">Welcome to Plex Requests</h4>
              </div>
              <div className="modal-body">
                  <strong>Thank you for installing and using Plex Requests!</strong>
                  <br />
                  <br />
                  Before you get started, please head over to the <Link to="admin">admin page</Link> and create an account.
                  You can then configure things and get going. Some highlights of the new version can be seen below:
                  <br />
                  <br />
                  <ul>
                    <li>
                      Approval system for requests
                    </li>
                    <li>
                      Enabled or disable search categories
                    </li>
                    <li>
                      Users can report issues with content
                    </li>
                    <li>
                      Improved visual style
                    </li>
                  </ul>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary-outline" data-dismiss="modal">Close</button>
              </div>
            </div>{/* .modal-content */}
          </div>{/* .modal-dialog */}
        </div>{/* .modal */}
      </div>
    )
  }
}