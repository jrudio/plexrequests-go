import React, {Component} from 'react'
import {Link} from 'react-router'

// Components
import Button from 'react-bootstrap/lib/Button'

export default class Home extends Component {
  componentWillMount () {
    // Move login check to a container or up one level
    // and pass in the login state via props
    this.setState({loggedin: true})
  }
  toggleLogin () {
    const currLoginState = this.state.loggedin

    this.setState({loggedin: currLoginState ? false : true})
  }
  loggedInHtml () {
    let countdown = 4

    setTimeout(() => {
      countdown--
    }, 1000)

    return (
      <div className="col-md-10 col-md-offset-1">
        <div className="home">
          <h1>You're all logged in!</h1>
          <br />
          <br />
          <h3>You will be redirected to the search page in {countdown} seconds!</h3>
          <br />
          <h3>If you want to view existing requests, there's a <Link to="requests">page</Link> for that.</h3>
        </div>
      </div>
    )
  }
  notLoggedInHtml () {
    let requirePassword = false
    let passwordHtml = undefined

    if (requirePassword) {
      passwordHtml = (
        <div className="form-group">
          <label htmlFor="plex-password">Password</label>
          <input type="password" className="form-control" id="plex-password" placeholder="Password" />
        </div>
      )
    }

    return (
      <div className="col-md-10 col-md-offset-1">
          <div className="home">
            <h1>Login</h1>

            <p>
              Want to watch a movie or tv show but it's not currently on Plex?
            </p>
            <p>
              Login below with your Plex.tv username and password! <span title="Your login details are only used to authenticate your Plex account."><i className="fa fa-question-circle"></i></span>
            </p>

            <form className="form col-md-6" id="sign-in">
              <fieldset>
                <div className="form-group">
                  <label htmlFor="plex-username">Plex.tv Username</label>
                  <input type="text" className="form-control" id="plex-username" placeholder="Username" />
                </div>
                {passwordHtml}
              </fieldset>
              <button className="btn btn-secondary-outline" type="submit" id="submitButton"><i className="fa fa-user fa-fw"></i> Sign In</button>
            </form>

          </div>
        </div>
    )
  }
  render () {
    let pageState

    if (this.state.loggedin) {
      pageState = this.loggedInHtml()
    } else {
      pageState = this.notLoggedInHtml()
    }

    return (
      <div>
        <h1>Home</h1>

        <div>
          <Button onClick={this.toggleLogin.bind(this)}>
            Toggle login
          </Button>
        </div>

        {pageState}
      </div>
    )
  }
}