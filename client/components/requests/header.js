import React, {Component} from 'react'

export default class Requests extends Component {
  render () {
    let currentUser = true
    let currentUserHtml = undefined

    if (currentUser) {
      currentUserHtml = (
        <p>
          If you want to approve all Movies and TV Shows, click here <button className="btn btn-secondary-outline btn-sm" id="approveAll">Approve All</button>
        </p>
      )
    }

    return (
      <div className="row">
        <div className="col-md-offset-1 col-md-10">
          <h1>Requests</h1>
          <p>Below you can see yours and all other requests, as well as their download and approval status.</p>
          {currentUser}
        </div>
      </div>
    )
  }
}