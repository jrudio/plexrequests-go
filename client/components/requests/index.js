import React, {Component} from 'react'
import {Link} from 'react-router'

// Bootstrap Components
import DropdownButton from 'react-bootstrap/lib/DropdownButton'

import Header from './header'

export default class Requests extends Component {
  get filterOptions () {

    return [
      {filter: "All Requests"},
      {filter: "Approved"},
      {filter: "Not Approved"},
      {filter: "Downloaded"},
      {filter: "Not Downloaded"},
      {filter: "Has Issues"}
    ]
  }
  render () {
    return (
        <div className="requests">

          {<Header />}

          {/* Selector between Movies/TV/etc  and Sort/Filter Options */}
            <div className="row selectors">
              <div className="col-md-offset-1 col-md-8">
                <ul className="search-selector">
                  {/* {{#each searchOptions}}
                                      <li><a href="#" className="{{#if activeSearch}}active-search{{/if}}">{{this}}</a></li>
                                    {{/each}}
                  */}
                </ul>
              </div>
              <div className="col-md-2">
                <div className="filter-sort">
                  <div className="btn-group">

                    <DropdownButton title="Filter / Sort" id="filter-sort">
                      {
                        this.filterOptions.forEach(filter => {
                          return (
                            <a className="dropdown-item filter-select" href="#">
                            {filter}
                            </a>
                          )
                        })
                      }
                    </DropdownButton>

                    <button type="button" className="btn btn-sm btn-secondary-outline dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Filter / Sort
                    </button>
                    <div className="dropdown-menu">
                      {/*
                          {{#each filterOptions}}
                            <a className="dropdown-item filter-select" href="#">{{{activeFilter}}}{{filter}}</a>
                          {{/each}}
                      */}
                      <div className="dropdown-divider"></div>
                        {
                          /*
                            {{#each sortOptions}}
                              <a className="dropdown-item sort-select" href="#">{{{activeSort}}}{{sort}}</a>
                            {{/each}}
                          */
                        }
                    </div>
                  </div>
                </div>
              </div>
            </div>

          {/* List of content */}

          {/*
            {{#each requests}}
          */}
            <div className="request-item">
                <div className="row">
                  <div className="col-md-2 col-md-offset-1">
                    {/*
                    <img src="{{poster_path}}" className="img-responsive" alt="Poster" />
                    */}
                  </div>
                  <div className="col-md-5">
                    <ul className="request-details-list">
                      {/*
                      {{#if searchType 'Movies'}}
                      
                        <li><a href="http://www.dereferer.org/?http://www.imdb.com/title/{{imdb}}" target="_blank"><strong>{{title}}</strong></a></li>
                      {{/if}}
                      {{#if searchType 'TV Shows'}}
                        <li><a href="http://www.dereferer.org/?http://thetvdb.com/?tab=series&id={{tvdb}}" target="_blank"><strong>{{title}}</strong></a></li>
                        {{/if}}
                      */}
                      <li><strong>Release date:</strong> {/*{{release_date}}*/}</li>
                      <li><strong>Approved:</strong> 
                      {/*
                      {{{approval_status}}}
                      */}
                      </li>
                      <li><strong>Available:</strong> 
                      {/*
                      {{{download_status}}}
                      */}
                      </li>
                      
                      {/*
                      {{{requesting_user}}}
                      */}
                      <li><strong>Issues:</strong> {/* {{issues}} */}</li>
                    </ul>
                  </div>
                  <div className="col-md-3 request-details-buttons">
                    {/*
                    {{#if currentUser}}
                      {{#unless approved}}<p><button className="btn btn-sm btn-success-outline approve-item">Approve</button></p>{{/unless}}
                    */}
                      <p><button className="btn btn-sm btn-danger-outline delete-item">Delete</button></p>
                      <p><button className="btn btn-sm btn-info-outline clear-issues">Clear Issues</button></p>

                    {/*
              
                      {{#if searchType 'Movies'}}
                        {{#if available}}
                          <p><button className="btn btn-sm btn-secondary-outline mark-unavailable">Mark Unavailable</button></p>
                        {{else}}
                    */}
                        <p><button className="btn btn-sm btn-success-outline mark-available">Mark Available</button></p>
                    {/*
                      {{/if}}
                      {{/if}}
                    {{/if}}
                    */}
                    <div className="btn-group">
                      <button type="button" className="btn btn-sm btn-warning-outline dropdown-toggle report-item" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Report Issue <span className="caret"></span>
                      </button>
                      <ul className="dropdown-menu">
                      <li><a href="#" className="issue-select dropdown-item" data="Wrong Audio">Wrong audio</a></li>
                      <li><a href="#" className="issue-select dropdown-item" data="No Subtitles">No subtitles</a></li>
                      <li><a href="#" className="issue-select dropdown-item" data="Wrong Content">Wrong content</a></li>
                      <li><a href="#" className="issue-select dropdown-item" data="Playback issues">Playback issues</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-10 col-md-offset-1">
                  <div className="item-divider"></div>
                </div>
              </div>
            {/*
              {{/each}}
            */}

            {/* Go to top button */}
            <div className="go-to-top">
              <i className="fa fa-arrow-up"></i>
            </div>

            {/* Load more event trigger */}
            <div className="load-more" style={{display: 'none'}}></div>

            <div className="row">
              <div className="col-md-10 col-md-offset-1">
              {/*
                {{#unless Template.subscriptionsReady}}
              */}
                  <h3 className="loading">Loading... <i className="fa fa-spin fa-refresh"></i></h3>
              {/*

                {{/unless}}
              */}
              </div>
            </div>
          </div>
    )
  }
}