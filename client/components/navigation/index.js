import React, {Component} from 'react'
import Link from 'react-router/lib/Link'

export default class Navigation extends Component {

  render () {
    let auth = true
    let plexAuth = true

    return (
      <nav className="navbar navbar-dark" style={{backgroundColor: "#000"}}>
          <button className="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#navBarItems">
            &#9776;
          </button>
          <div className="collapse navbar-toggleable-sm" id="navBarItems">
            <Link className="navbar-brand" to="home"><i className="fa fa-play-circle logo-icon"></i> Plex Requests</Link>
            <Link className="navbar-brand admin-link" to="admin">Admin</Link>
            <ul className="nav navbar-nav pull-xs-right">
              {(() => {
                if (plexAuth) {
                  return <li className="nav-item"><Link to="search" activeClassName="active" className="nav-link">Search</Link></li>
                }
              })()}


              {(() => {
                if (auth) {
                  return <li className="nav-item"><Link to="requests" activeClassName="active" className="nav-link">Requests</Link></li>
                }
              })()}

              {(() => {
                if (!plexAuth) {
                  return <li className="nav-item"><Link to="home" className="nav-link">Login</Link></li>
                }
              })()}


              {(() => {
                if (plexAuth) {
                  return <li className="nav-item"><Link to="#" id="logoutUser" className="nav-link">Logout</Link></li>
                }
              })()}

            </ul>
          </div>
        </nav>
    )
  }
}
