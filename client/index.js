import Run from './router'

let removeServerLoader = function () {
  let body = document.body

  let el = document.getElementById('server-loader')

  body.removeChild(el)
}

let loadCss = () => {
  require('./css')
}

let loadBootstrapJS = () => {
  window.jQuery = require('jquery')
  window.Tether = require('tether/dist/js/tether.js')
  require('bootstrap/dist/js/bootstrap.min.js')
}

Run(() => {
  // After the router runs
  removeServerLoader()
  loadCss()
  loadBootstrapJS()
})
