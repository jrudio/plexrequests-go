import React from 'react'
import {Route, IndexRoute, Redirect} from 'react-router'
import {Link} from 'react-router'

// Import Components
import Admin from '#app/components/admin'
import Home from '#app/components/home'
import Layout from '#app/components/layout'
import Requests from '#app/components/requests'
import Search from '#app/components/search'

// Dev: Register these routes on the server
export function ImportRoutes () {
  return (
    <Route path="/" component={Layout}>
      <IndexRoute component={Home} />
      <Route path="/admin" component={Admin} />
      <Route path="/requests" component={Requests} />
      <Route path="/search" component={Search} />
    </Route>
  )
}
