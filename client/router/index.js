import React from 'react'
import {render} from 'react-dom'
import browserHistory from 'react-router/lib/browserHistory'
import Link from 'react-router/lib/Link'
import Route from 'react-router/lib/Route'
import Router from 'react-router/lib/Router'


import {ImportRoutes} from './routes'


export default function run (cb = () => {}) {
  let reactAppNode = document.getElementById('app')

  render(
    <Router history={browserHistory}>
      {ImportRoutes()}
    </Router>
  , reactAppNode)

  cb()
}
