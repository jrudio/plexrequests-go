package main

import (
	"flag"
	// "fmt"
	// "github.com/boltdb/bolt"
	// "github.com/graphql-go/graphql"
	"github.com/elazarl/go-bindata-assetfs"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/itsjamie/go-bindata-templates"
	"github.com/jrudio/plexrequests/server/bin_templates"
	"html/template"
	"log"
	"net/http"
	"strings"
)

var (
	host   string
	templs *template.Template = binhtml.New(templatesBin.Asset, templatesBin.AssetDir).MustLoadDirectory("templates")
)

type binaryFileSystem struct {
	fs http.FileSystem
}

func (b *binaryFileSystem) Open(name string) (http.File, error) {
	return b.fs.Open(name)
}

func (b *binaryFileSystem) Exists(prefix string, filepath string) bool {
	if p := strings.TrimPrefix(filepath, prefix); len(p) < len(filepath) {
		if _, err := b.fs.Open(p); err != nil {
			return false
		}
		return true
	}
	return false
}

func BinaryFileSystem(root string) *binaryFileSystem {
	fs := &assetfs.AssetFS{Asset, AssetDir, AssetInfo, root}
	return &binaryFileSystem{
		fs,
	}
}

func main() {
	initFlags()

	// data, dataErr := templatesBin.Asset("templates/react.html")

	// if dataErr != nil {
	// 	log.Println("Asset error: " + dataErr.Error())
	// 	return
	// }

	// log.Println(string(data))
	// log.Println(assetFS())

	// return

	router := gin.Default()

	// Static assets directory
	router.Use(static.Serve("/static", BinaryFileSystem("data/static")))

	router.GET("/", handleReact)
	router.GET("/users", handleReact)
	router.GET("/search", handleReact)
	router.GET("/requests", handleReact)
	router.GET("/admin", handleReact)

	router.GET("/graphql", handleGraphQL)

	router.Run(host)
}

func handleReact(c *gin.Context) {
	if execErr := templs.Execute(c.Writer, gin.H{}); execErr != nil {
		log.Println("Exec template error: " + execErr.Error())
		c.AbortWithStatus(http.StatusInternalServerError)
	}
}

func handleGraphQL(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"query": c.Request.URL.Query()})
}

// func executeQuery(query string, schema graphql.Schema) *graphql.Result {
//   result := graphql.Do(graphql.Params{
//     Schema:        schema,
//     RequestString: query,
//   })
//   if len(result.Errors) > 0 {
//     fmt.Printf("wrong result, unexpected errors: %v", result.Errors)
//   }
//   return result
// }

func initFlags() {
	flag.StringVar(&host, "host", ":4040", "host to listen on")

	flag.Parse()
}
