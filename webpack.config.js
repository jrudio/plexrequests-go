var path = require('path')
var spawn = require('child_process').spawnSync
var webpack = require('webpack')
var WebpackOnBuildPlugin = require('on-build-webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var plugins = [
  new webpack.optimize.DedupePlugin(),
  new WebpackOnBuildPlugin(removeOldAssets),
  new WebpackOnBuildPlugin(compileTemplate),
  new WebpackOnBuildPlugin(compileStaticAsset),
  new ExtractTextPlugin('bundle.css')
]

var bindataWrkDir = path.join(__dirname, 'server')
var templateFile = path.join('bin_templates', 'template_bindata.go')
var assetFile = 'bindata_assetfs.go'

function removeOldAssets (stat) {
  var rmAssets = spawn('rm', [templateFile, '&&', 'rm', assetFile], {cwd: bindataWrkDir})
  var stdout = rmAssets.output[1].toString('utf8')
  var stderr = rmAssets.output[2].toString('utf8')

  if (stderr.length > 0) {
    console.log('removeAssets error: ')
    console.log(stderr)
  }

  if (stdout.length > 0) {
    console.log('removeAssets: ')
    console.log(stdout)
  }
}

function compileTemplate (stat) {
  var bindataArgs = ['-o', templateFile, '-pkg', 'templatesBin',
   '-prefix', 'data', 'data/templates/...']

  var bindata = spawn('go-bindata', bindataArgs, {cwd: bindataWrkDir})

  var stdout = bindata.output[1].toString('utf8')
  var stderr = bindata.output[2].toString('utf8')

  if (stderr.length > 0) {
    console.log('go-bindata error: ')
    console.log(stderr)
  }

  if (stdout.length > 0) {
    console.log('go-bindata: ')
    console.log(stdout)
  }
}

function compileStaticAsset (stat) {
  var bindata = spawn('go-bindata-assetfs', ['data/static/...'], {cwd: bindataWrkDir})

  var stdout = bindata.output[1].toString('utf8')
  var stderr = bindata.output[2].toString('utf8')


  if (stderr.length > 0) {
    console.log('go-bindata-assetfs error: ')
    console.log(stderr)
  }

  if (stdout.length > 0) {
    console.log('go-bindata-assetfs: ')
    console.log(stdout)
  }
}

module.exports = {
  plugins,
  entry: {
    bundle: path.join(__dirname, 'client', 'index.js')
  },
  output: {
    path: path.join(__dirname, 'server', 'data', 'static', 'build'),
    publicPath: path.join('static', 'build') + '/',
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules)|(\.min\.js$)/,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css!sass')
      },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" }

    ]
  },
  resolve: {
    extensions: ['', '.js', '.css'],
    alias: {
      '#app': path.join(__dirname, 'client'),
      '#c': path.join(__dirname, 'client', 'components'),
      '#css': path.join(__dirname, 'client', 'css')
    }
  }
}