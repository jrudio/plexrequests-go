# Plex Requests built with Go
A Go port of plexrequests. For cross-platform support without external dependencies. You can follow the [dev](https://github.com/jrudio/plexrequests-go/tree/dev) branch, while I try to get this port on par with [plexrequests-meteor](https://github.com/lokenx/plexrequests-meteor)'s core features.

###Current Features 

###Planned Features / Goals

- Localization 
- A companion app for ios and Android
- A smaller memory footprint than what meteor (node) uses

######Note

- Repo structure is based upon [go-starter-kit](https://github.com/olebedev/go-starter-kit)


######Building from source
- If building from source please ensure you have [govendor](https://github.com/kardianos/govendor) installed for the correct package versions

- Make sure you have [go-bindata]() and [go-bindata-assetfs]() in your $PATH as webpack compiles static assets and templates

Ideal distribution structure

```bash
/plexrequests_<os name>
    pr.db
    plexrequests // executable
```
